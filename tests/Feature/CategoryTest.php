<?php

namespace Tests\Feature;

use App\Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    protected const DATABASE_NAME = 'categories';

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Get all categorys as list
     *
     * @return void
     */
    public function testCanGetCategoryList()
    {
        $categorys = factory(Category::class, 10)->create();

        $response = $this->json('GET', '/api/category');

        $response
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                    ],
                ],
            ])
            ->assertStatus(200);
    }

    /**
     * Get specific category
     *
     * @return void
     */
    public function testCanGetSpecificCategory()
    {
        $category = factory(Category::class)->create();

        $response = $this->json('GET', "/api/category/{$category->id}");

        $response
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                ],
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas(self::DATABASE_NAME, $category->toArray());
    }

    /**
     * Create category
     *
     * @return void
     */
    public function testCanCreateCategory()
    {
        $this->withoutExceptionHandling();

        $category = factory(Category::class)->make()->toArray();

        $response = $this->json('POST', '/api/category', $category);
        $response->assertStatus(201);

        $this->assertDatabaseHas(self::DATABASE_NAME, $category);
    }

    /**
     * Create Empty category
     *
     * @return void
     */
    public function testCantCreateEmptyCategory()
    {
        $response = $this->json('POST', '/api/category', []);
        $response->assertStatus(422);
    }

    /**
     * Update specific category
     *
     * @return void
     */
    public function testCanUpdateSpecificCategory()
    {
        $this->withoutExceptionHandling();

        $category = factory(Category::class)->create();
        $updates = factory(Category::class)->make()->toArray();

        $response = $this->json('PUT', "/api/category/{$category->id}", $updates);
        $response->assertStatus(200);

        $this->assertDatabaseHas(self::DATABASE_NAME, $updates);
    }

    /**
     * Delete specific category
     *
     * @return void
     */
    public function testCanDeleteSpecificCategory()
    {
        $this->withoutExceptionHandling();

        $category = factory(Category::class)->create();

        $response = $this->json('DELETE', "/api/category/{$category->id}");
        $response->assertStatus(200);

        $this->assertDatabaseMissing(self::DATABASE_NAME, $category->toArray());
    }

}
