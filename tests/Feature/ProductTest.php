<?php

namespace Tests\Feature;

use App\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    protected const DATABASE_NAME = 'products';

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Get all products as list
     *
     * @return void
     */
    public function testCanGetProductList()
    {
        $products = factory(Product::class, 10)->create();

        $response = $this->json('GET', '/api/product');

        $response
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                    ],
                ],
            ])
            ->assertStatus(200);
    }

    /**
     * Get specific product
     *
     * @return void
     */
    public function testCanGetSpecificProduct()
    {
        $product = factory(Product::class)->create();

        $response = $this->json('GET', "/api/product/{$product->id}");

        $response
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                ],
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas(self::DATABASE_NAME, $product->toArray());
    }

    /**
     * Create product
     *
     * @return void
     */
    public function testCanCreateProduct()
    {
        $this->withoutExceptionHandling();

        $product = factory(Product::class)->make()->toArray();

        $response = $this->json('POST', '/api/product', $product);
        $response->assertStatus(201);

        $this->assertDatabaseHas(self::DATABASE_NAME, $product);
    }

    /**
     * Create Empty product
     *
     * @return void
     */
    public function testCantCreateEmptyProduct()
    {
        $response = $this->json('POST', '/api/product', []);
        $response->assertStatus(422);
    }

    /**
     * Update specific product
     *
     * @return void
     */
    public function testCanUpdateSpecificProduct()
    {
        $this->withoutExceptionHandling();

        $product = factory(Product::class)->create();
        $updates = factory(Product::class)->make()->toArray();

        $response = $this->json('PUT', "/api/product/{$product->id}", $updates);
        $response->assertStatus(200);

        $this->assertDatabaseHas(self::DATABASE_NAME, $updates);
    }

    /**
     * Delete specific product
     *
     * @return void
     */
    public function testCanDeleteSpecificProduct()
    {
        $this->withoutExceptionHandling();

        $product = factory(Product::class)->create();

        $response = $this->json('DELETE', "/api/product/{$product->id}");
        $response->assertStatus(200);

        $this->assertDatabaseMissing(self::DATABASE_NAME, $product->toArray());
    }

}
