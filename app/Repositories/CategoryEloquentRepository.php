<?php

namespace App\Repositories;

use App\Category;

class CategoryEloquentRepository extends AbstractEloquentRepository implements CategoryRepositoryInterface
{

    protected $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }

}
