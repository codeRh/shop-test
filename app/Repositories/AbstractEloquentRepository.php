<?php

namespace App\Repositories;

abstract class AbstractEloquentRepository
{

    /**
     * @var
     */
    protected $model;

    /**
     * Return specific model
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function find($id, $columns = array('*'))
    {
        return $this->model->find($id, $columns);
    }

    public function findOrFail($id, $columns = array('*'))
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function findWith($id, array $with)
    {
        return $this->with($with)->where('id', $id)->first();
    }

    public function findWithOrFail($id, array $with)
    {
        return $this->with($with)->where('id', $id)->firstOrFail();
    }

    /**
     * Return all models
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all($columns = array('*'))
    {
        return $this->model->all($columns);
    }

    public function with(...$relations)
    {
        return $this->model->with(...$relations);
    }

    public function load(...$relations)
    {
        return $this->model->load(...$relations);
    }

    public function create(array $attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes, array $options = [])
    {
        $model = $this->find($id);
        $model->update($attributes, $options);
        return $model;
    }

    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

}
