<?php

namespace App\Repositories;

interface CategoryRepositoryInterface
{

    public function all($columns = array('*'));
    public function find($id, $columns = array('*'));
    public function findOrFail($id, $columns = array('*'));
    public function findWith($id, array $with);
    public function findWithOrFail($id, array $with);
    public function with(...$relations);
    public function load(...$relations);
    public function create(array $attributes = []);
    public function update($id, array $attributes, array $options = []);
    public function destroy($id);

}
