<?php

namespace App\Repositories;

use App\Product;

class ProductEloquentRepository extends AbstractEloquentRepository implements ProductRepositoryInterface
{

    protected $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }

}
