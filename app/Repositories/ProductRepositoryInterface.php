<?php

namespace App\Repositories;

interface ProductRepositoryInterface
{

    public function find($id, $columns = array('*'));
    public function findOrFail($id, $columns = array('*'));
    public function all($columns = array('*'));
    public function with(...$relations);
    public function create(array $attributes);
    public function update($id, array $attributes);
    public function destroy($id);

}
