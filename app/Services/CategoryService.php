<?php

namespace App\Services;

use App\Http\Requests\StoreCategory;
use App\Repositories\CategoryRepositoryInterface;

class CategoryService
{

    protected $repository;

    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Returns a listing of the resource as node tree.
     *
     */
    public function index()
    {
        return $this->repository->all()->toHierarchy();
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(StoreCategory $request)
    {
        return $this->repository->create($request->validated());
    }

    /**
     * Returns the specified resource.
     *
     */
    public function show($id)
    {
        return $this->repository->findWith($id, ['children', 'products']);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update($id, StoreCategory $request)
    {
        return $this->repository->update($id, $request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        return $this->repository->destroy($id);
    }

}
