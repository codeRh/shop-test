<?php namespace App\Services;

use App\Http\Requests\StoreProduct;
use App\Repositories\ProductRepositoryInterface;

class ProductService
{

    /**
     * Repository instance
     */
    protected $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(StoreProduct $request)
    {
        return $this->repository->create($request->validated());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        return $this->repository->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update($id, StoreProduct $request)
    {
        return $this->repository->update($id, $request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        return $this->repository->destroy($id);
    }

}
