<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\ProductRepositoryInterface::class, 
            \App\Repositories\ProductEloquentRepository::class
        );

        $this->app->bind(
            \App\Repositories\CategoryRepositoryInterface::class, 
            \App\Repositories\CategoryEloquentRepository::class
        );
    }
}
