<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $timestamps = false;

    protected $fillable = ['title', 'description', 'price', 'quantity', 'category_id'];

    public function category() {
        return $this->belongsTo(Category::class);
    }

}
