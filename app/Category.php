<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class Category extends Node
{    
    protected $fillable = array('title', 'slug', 'description');
    
    // NestedSet
    protected $parentColumn = 'parent_id';
    protected $leftColumn = 'left';
    protected $rightColumn = 'right';
    protected $depthColumn = 'level';
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function products() {
        return $this->hasMany(Product::class);
    }

    public function nestedProducts() {
        $ids = $this->getDescendantsAndSelf()->pluck('id')->all();
        return Product::whereIn('category_id', $ids)->get();
    }
    
}
