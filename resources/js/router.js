import Vue from 'vue'
import Router from 'vue-router'
import Shop from './components/Shop.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [{
        path: '/',
        name: 'home',
        component: Shop
    }, {
        path: '/category/:id',
        name: 'category',
        props: true,
        component: Shop
    }]
})