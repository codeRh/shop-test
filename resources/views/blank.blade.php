<!-- 

    1. Тестовое задание на Laravel

    Даны две сущности: категория и товар
    Категории сделать при помощи sql паттерна nested set. Можно взять пакет для этого. 
    Товары нужно помещать в категории. 
    У товара должно быть название, описание и изображение
    Все crud операции, связанные с товарами и категориями должны быть доступны через API 
    Сделать фронтенд на vuejs в виде spa который слева будет отображать дерево категорий, при нажатии подгружать товары из соответствующей категории.
    Написать seeds и factory для создания фейковых категорий и товаров в них.

    Код должен соответстовать PSR-2.

    Nested set пакет: https://github.com/etrepat/baum

    Фронтенд: vuejs 2, vue-router, vue-resource. Для верстки давай bulma.io. Сборка посредством webpack.
    Отличный пример конфига webpack сможешь найти в репозитории vue-admin.
    Если тебе будет проще, то можешь воспользоваться laravel-mix webpack для сборки.
    https://pastebin.com/EJyKjycP это пример Юнит не обязательно, но если справишься будет супер

-->

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Shop</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    </head>
    <body>
        <div id="app"></div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
    </body>
</html>
