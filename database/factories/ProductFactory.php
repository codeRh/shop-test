<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {

    $image = (app()->environment() == 'testing') ? null : $faker->image( public_path().'/images', 640, 480, null, false);

    return [
        'title' => ucfirst($faker->word(1)),
        'description' => $faker->text(60),
        'price' => rand(0, 1000) / 100,
        'image' => $image,
        'quantity' => rand(0, 10),
    ];

});
