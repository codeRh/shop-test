<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {

    return [
        'title' => ucfirst($faker->word( round(rand(1, 2))) ),
        'description' => $faker->text(60),
        // 'created_at' => $faker->dateTimeBetween('-1 year', '-1 day'),
    ];

});
