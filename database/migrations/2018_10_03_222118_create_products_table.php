<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('title');
            
            $table->text('description')->default('');
            
            $table->integer('category_id')->unsigned()->nullable();
            // $table->foreign('category_id')->references('id')->on('categories');
            
            $table->string('image')->nullable();
            
            $table->integer('quantity')->unsigned()->default(0);
            
            $table->decimal('price', 6, 2)->unsigned()->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
