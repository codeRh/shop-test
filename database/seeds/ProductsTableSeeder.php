<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $count = App\Category::count();

        factory(App\Product::class, $count * 3)->create()->each(function($product) use ($count) {
            $product->category_id = rand(1, $count);
            $product->save();
        });
    }
}
