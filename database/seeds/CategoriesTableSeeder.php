<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Category::class, 2)->create()->each(function($category) {
            $category->children()->saveMany(
                factory(App\Category::class, rand(0, 5))->create()->each(function($category) {
                    $category->children()->saveMany(
                        factory(App\Category::class, rand(0, 3))->create()
                    );
                })
            );
        });

        App\Category::rebuild(1);

    }
}
